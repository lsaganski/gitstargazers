//
//  Network.swift
//  GitStargazers
//
//  Created by Leonardo Saganski on 1/9/20.
//  Copyright © 2020 Mobila. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case urlError
    case requestError
    case noData
    case decodingError
    
    func getMessage() -> String {
        switch self {
        case .urlError:
            return "Erro ao processar a URL"
        case .requestError:
            return "Erro na requisição da API"
        case .noData:
            return "Nenhum dado retornou da API"
        case .decodingError:
            return "Erro ao decodificar o resultado da requisição"
        }
    }
}

class Network: NSObject {
    static let shared = Network()
    
    func get<T: Codable>(strUrl: String, completion: @escaping (Result<T, NetworkError>) -> Void) {
        guard let url = URL(string: strUrl) else {
            completion(.failure(.urlError))
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            if error != nil {
                completion(.failure(.requestError))
                return
            }
            
            guard let data = data else {
                completion(.failure(.noData))
                return
            }
            
            guard let parsedData = try? JSONDecoder().decode(T.self, from: data) else {
                completion(.failure(.decodingError))
                return
            }
            
            completion(.success(parsedData))
        }.resume()
    }

    func getImage(fromUrl: String, completion: @escaping (Bool, Data?) -> Void) {
        guard let url = URL(string: fromUrl) else {
            completion(false, nil)
            return
        }

        URLSession.shared.dataTask(with: url) {
            data, response, error in
            
            if error != nil {
                completion(false, nil)
                return
            }
            
            guard let data = data else {
                completion(false, nil)
                return
            }
            
            completion(true, data)
        }.resume()
    }

    func getRepos(page: Int, completion: @escaping (Result<RepoResponse, NetworkError>) -> Void) {
        let strUrlBase = "https://api.github.com/search/repositories"
        let params = "?q=language:swift&sort=stars&per_page=20&page="

        let strUrl = "\(strUrlBase)\(params)\(page)"
        
        get(strUrl: strUrl, completion: completion)
    }
}
