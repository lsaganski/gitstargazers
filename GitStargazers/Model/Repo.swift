//
//  Repo.swift
//  GitStargazers
//
//  Created by Leonardo Saganski on 1/9/20.
//  Copyright © 2020 Mobila. All rights reserved.
//

struct RepoResponse: Codable {
    var total_count: Int?
    var incomplete_results: Bool?
    var items: [Repo]?
}

struct Repo: Codable {
    var id: Int?
    var node_id: String?
    var name: String?
    var owner: Owner?
    var stargazers_count: Int?
}

struct Owner: Codable {
    var id: Int?
    var login: String?
    var avatar_url: String?
}
