//
//  BaseViewController.swift
//  GitStargazers
//
//  Created by Leonardo Saganski on 1/9/20.
//  Copyright © 2020 Mobila. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    func showErrorMessage(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        getWindow().rootViewController?.present(alert, animated: true)
    }
    
    func getWindow() -> UIWindow {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1
        alertWindow.makeKeyAndVisible()
        
        return alertWindow
    }
}

