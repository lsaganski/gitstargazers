//
//  RepoViewModel.swift
//  GitStargazers
//
//  Created by Leonardo Saganski on 1/9/20.
//  Copyright © 2020 Mobila. All rights reserved.
//

import Foundation

class RepoViewModel: NSObject {
    private var list: [Repo] = []
    
    func listCount() -> Int {
        return self.list.count
    }
    
    func get(at index: Int) -> Repo {
        return self.list[index]
    }

    func loadData(page: Int, completion: @escaping (Bool, String?) -> Void) {
        Network().getRepos(page: page) {
            [weak self]
            result in
            
            guard let strongSelf = self else {
                return
            }
            
            switch result {
            case .success(let response):
                if page == 1 {
                    strongSelf.list = response.items ?? []
                } else {
                    strongSelf.list.append(contentsOf: response.items ?? [])
                }
                completion(true, nil)
            case .failure(let error) :
                completion(false, error.getMessage())
            }
        }
    }
}
