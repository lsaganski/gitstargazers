//
//  RepoViewController.swift
//  GitStargazers
//
//  Created by Leonardo Saganski on 1/9/20.
//  Copyright © 2020 Mobila. All rights reserved.
//

import Foundation
import UIKit

class RepoViewController: BaseViewController {
    private var tableView: UITableView?
    private let refreshControl = UIRefreshControl()
    private var loading: Loading?
    private var isLoading = false
    private var page = 1
    private let viewModel = RepoViewModel()
    
    override func viewDidLoad() {
        setupUI()
        loading = Loading(view: self.view)
        tableView?.register(RepoTableViewCell.self, forCellReuseIdentifier: "repoCell")
        tableView?.register(LoadingTableViewCell.self, forCellReuseIdentifier: "loadingCell")
    }
    
    override func viewWillAppear(_ animate: Bool) {
        page = 1
        loadData(page: page)
    }
    
    private func setupUI() {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
            ])
        self.tableView = tableView

        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        refreshControl.tintColor = .red
        refreshControl.attributedTitle = NSAttributedString(string: "Carregando dados...")
        self.tableView?.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(onRefreshData), for: .valueChanged)
    }
    
    @objc
    private func onRefreshData() {
        page = 1
        loadData(page: page)
    }
    
    private func loadData(page: Int) {
        if !isLoading {
            isLoading = true
            loading?.start()
            viewModel.loadData(page: page) {
                [weak self]
                success, errorMessage in
                
                guard let strongSelf = self else {
                    return
                }
                
                DispatchQueue.main.async {
                    strongSelf.loading?.stop()
                    strongSelf.isLoading = false
                    strongSelf.refreshControl.endRefreshing()
                    if success {
                        strongSelf.tableView?.reloadData()
                    } else {
                        self?.showErrorMessage(message: errorMessage ?? "")
                    }
                }
            }
        }
    }
}

extension RepoViewController: UITableViewDataSource, UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading {
            page += 1
            loadData(page: page)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.listCount()
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "repoCell", for: indexPath) as? RepoTableViewCell {
                let item = viewModel.get(at: indexPath.row)
                cell.setData(name: item.name, stars: item.stargazers_count, owner: item.owner?.login, avatarLink: item.owner?.avatar_url)
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath) as? LoadingTableViewCell {
                cell.activity.startAnimating()
                return cell
            }
        }
        
        return UITableViewCell()
    }
}
