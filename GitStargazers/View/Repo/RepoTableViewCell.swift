//
//  RepoTableViewCell.swift
//  GitStargazers
//
//  Created by Leonardo Saganski on 1/9/20.
//  Copyright © 2020 Mobila. All rights reserved.
//

import Foundation
import UIKit

class RepoTableViewCell: UITableViewCell {
    let viewContainer = UIView(frame: .zero)
    let labelName = UILabel(frame: .zero)
    let labelStars = UILabel(frame: .zero)
    let labelOwner = UILabel(frame: .zero)
    let imgAvatar = UIImageView(frame: .zero)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        viewContainer.translatesAutoresizingMaskIntoConstraints = false
        labelName.translatesAutoresizingMaskIntoConstraints = false
        labelStars.translatesAutoresizingMaskIntoConstraints = false
        labelOwner.translatesAutoresizingMaskIntoConstraints = false
        imgAvatar.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(viewContainer)
        viewContainer.addSubview(labelName)
        viewContainer.addSubview(labelStars)
        viewContainer.addSubview(labelOwner)
        viewContainer.addSubview(imgAvatar)
        
        labelName.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        labelName.textColor = .darkGray
        labelStars.font = UIFont.systemFont(ofSize: 12, weight: .light)
        labelStars.textColor = .darkGray
        labelOwner.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        labelOwner.textColor = .darkGray

        NSLayoutConstraint.activate([
            viewContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            viewContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            viewContainer.topAnchor.constraint(equalTo: self.topAnchor),
            viewContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            labelName.topAnchor.constraint(equalTo: viewContainer.topAnchor, constant: 8),
            labelName.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 8),
            labelName.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: 8),
            labelStars.topAnchor.constraint(equalTo: labelName.bottomAnchor, constant: 4),
            labelStars.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 8),
            imgAvatar.topAnchor.constraint(equalTo: labelStars.bottomAnchor, constant: 4),
            imgAvatar.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 8),
            imgAvatar.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor, constant: -4),
            imgAvatar.widthAnchor.constraint(equalToConstant: 20),
            imgAvatar.heightAnchor.constraint(equalToConstant: 20),
            labelOwner.bottomAnchor.constraint(equalTo: imgAvatar.bottomAnchor, constant: 0),
            labelOwner.leadingAnchor.constraint(equalTo: imgAvatar.trailingAnchor, constant: 8),
            labelOwner.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: 8)
            ])
    }
    
    func setData(name: String?, stars: Int?, owner: String?, avatarLink: String?) {
        labelName.text = name ?? ""
        labelStars.text = "\(stars ?? 0) stars"
        labelOwner.text = owner ?? ""
        imgAvatar.image = #imageLiteral(resourceName: "user_placeholder")
        Network().getImage(fromUrl: avatarLink ?? "") {
            [weak self]
            success, data in
            
            guard let strongSelf = self else {
                return
            }
            
            if success {
                if let data = data {
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        strongSelf.imgAvatar.image = image
                    }
                }
            }
        }
    }
}

class LoadingTableViewCell: UITableViewCell {
    let viewContainer = UIView(frame: .zero)
    let activity = UIActivityIndicatorView(frame: .zero)

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupUI() {
        activity.style = .medium
        
        viewContainer.translatesAutoresizingMaskIntoConstraints = false
        activity.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(viewContainer)
        viewContainer.addSubview(activity)
        
        NSLayoutConstraint.activate([
            viewContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            viewContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            viewContainer.topAnchor.constraint(equalTo: self.topAnchor),
            viewContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            activity.topAnchor.constraint(equalTo: viewContainer.topAnchor, constant: 8),
            activity.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor, constant: -8),
            activity.centerYAnchor.constraint(equalTo: viewContainer.centerYAnchor),
            activity.centerXAnchor.constraint(equalTo: viewContainer.centerXAnchor)
            ])
    }
}
